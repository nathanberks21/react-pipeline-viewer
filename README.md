# GitLab Pipeline Visualiser

The aim of this project is to provide a simple at-a-glance view of what CI pipelines are running at any one time.

The overview is split into two main routes:

**/**
Displays each CI node as a card. Each node is pooled into groups. As a CI nodes begins to run it moves to a running state and is further grouped by the project it is running for.

Details of the number of runs/day a node has run for is displayed on each card, as well as a link to the GitLab information for that node on GitLab.

**/summary**
Displayed block cards for each specified project. Allowed developers to see the number of pipelines currently running for a given project, and the number that are queued awaiting a CI node to free up.

## Running as a Developer

The instance requires a Python server to connect to the data correctly. This isn't available (right now) so running the application will not produce any actual results, unfortunately.

Install dependencies
`npm i`

Run development server
`npm run dev`

## Building the Application

Install dependencies
`npm i`

Build
`npm run build`

Output is located inside the `dist` folder.

## Technologies

The app is built using `react` as a framework, with `redux` to handle the application state and interaction with the API.

`webpack` is used to handle the build process, with `webpack-dev-server` providing the development environment with hot-reloading.

The application takes advantage of modular css. Css is imported directly into each component and is then only applied within that namespace, without any css-creep to other components.