import 'font-awesome-webpack';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter, Route } from 'react-router-dom'

import store from './store.js';

import RunnerOverview from './scenes/runnerOverview/RunnerOverview.jsx';
import RunnerDetails from './scenes/runnerDetails/RunnerDetails.jsx';
import SummaryOverview from './scenes/summaryOverview/SummaryOverview.jsx';

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <div>
                <Route exact path="/" component={RunnerOverview}/>
                <Route exact path="/details/:id" component={RunnerDetails}/>
                <Route exact path="/summary" component={SummaryOverview}/>
            </div>
        </HashRouter>
    </Provider>,
    document.getElementById('root')
);