import axios from 'axios';

// These are assigned to a specific domain, and should really be assigned
// at build time dependent on a target, using environment vars.
const PROJECTS_API = 'http://devserve03:3453/ci/api/v1.0/projects';
const RUNNERS_API = 'http://devserve03:3453/ci/api/v1.0/runners';
const QUEUED_API = 'http://devserve03:3453/ci/api/v1.0/unassigned_jobs';
const RUNNER_API = 'http://devserve03:3453/ci/api/v1.0/runner/';

export function getRunners() {
    return function(dispatch) {
        dispatch({type: 'GET_RUNNERS_START'});
        let projects;
        let runners;
        let queue;
        axios.get(PROJECTS_API)
            .then(({data}) => {
                projects = data.projects;
                return axios.get(RUNNERS_API);
            })
            .then(({data}) => {
                runners = data.runners;
                return axios.get(QUEUED_API);
            })
            .then(({data}) => {
                queue = data.unassigned_jobs;
                dispatch({type: 'GET_RUNNERS', data: {projects, runners, queue}});
            })
            .catch(() => dispatch({type: 'GET_RUNNERS_ERROR'}))
        ;
    }
}

export function getRunner(id) {
    return function(dispatch) {
        dispatch({type: 'GET_RUNNER_START'});
        axios.get(`${RUNNER_API}${id}`)
            .then(({data}) => {
                const jobs = data.jobs;
                const runner = data.runner;
                dispatch({type: 'GET_RUNNER', data: {jobs, runner}});
            })
            .catch(() => dispatch({type: 'GET_RUNNER_ERROR'}))
        ;
    }
}

export function getProjects() {
    return function(dispatch) {
        dispatch({type: 'GET_PROJECTS_START'});
        axios.get(PROJECTS_API)
            .then(({data}) => {
                const projects = data.projects;
                dispatch({type: 'GET_PROJECTS', data: {projects}});
            })
            .catch(() => dispatch({type: 'GET_PROJECTS_ERROR'}))
        ;
    }
}