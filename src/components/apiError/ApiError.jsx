import React from 'react';

import styles from './ApiError.css';

export default class ApiError extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <img className={styles.image} src="/images/no-connection.png"/>
                <div>
                    <h1>Unable to get GitLab data</h1>
                    <h3>Is the server turned on?</h3>
                </div>
            </div>
        );
    }
}