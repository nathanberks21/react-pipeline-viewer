import React from 'react';

import styles from './Loader.css';

export default class Header extends React.Component {
    render() {
        return (
            <div className={`${styles.loading} ${this.props.visible ? styles.visible : ''}`}>
                <p>Loading data</p>
                <i className={`fa fa-circle-o-notch fa-spin`} aria-hidden="true"></i>
            </div>
        );
    }
}