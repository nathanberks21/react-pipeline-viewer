import React from 'react';
import { Link } from 'react-router-dom';
import styles from './RunnerCard.css';

const TIME_INTERVAL = 1000;

export default class RunnerCard extends React.Component {
    render() {
        return (
            <div className={`${styles.runner} ${this.props.active ? styles.active : styles.inactive}`}>
                <div className={styles.info}>
                    <div className={styles.header}>
                        <Link to={`/details/${this.props.id}`} className={styles.description} title={this.props.description}>{this.props.description}</Link>
                        <a href={`${this.props.url}/runners/${this.props.id}`} target="_blank">
                            <i className={`fa fa-android ${styles.icon}`} aria-hidden="true" title="View runner on GitLab"></i>
                        </a>
                        <i className={`${this._resultIcon()} ${styles.icon}`} aria-hidden="true" title="Result"></i>
                        <i className={`fa fa-rocket ${styles.icon} ${styles.rocket} ${this.props.running ? styles.running : ''}`} aria-hidden="true" title="Currently running"></i>
                        <div className={styles.label} title="Mean jobs per day">{this.props.jobsPerDay}</div>
                    </div>
                    <div className={styles.job}>
                        {this._activeJob()}
                    </div>
                </div>
            </div>
        );
    }

    _activeJob() {
        return (
            <div>
                {<a  href={this.props.jobUrl} target="_blank">
                    <div>{this.props.reference}</div>
                    <div>{this.props.name}</div>
                </a>}
                <div>{this.props.time}</div>
            </div>
        );
    }

    _resultIcon() {
        if (!this.props.running && this.props.runResult) {
            if (this.props.runResult === 'success' ) {
                return `fa fa-check ${styles.green}`;
            } else if (this.props.runResult === 'failed' ) {
                return `fa fa-close ${styles.red}`;
            } else if (this.props.runResult === 'canceled' ) {
                return `fa fa-minus ${styles.grey}`;
            }
        } else {
            return styles.hidden;
        }
    }
}