import React from 'react';
import { connect } from 'react-redux';

import styles from './Sidebar.css';

@connect((store) => {
    return {
        runners: store.runners
    }
})
export default class Sidebar extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <div className={styles.header}>Pending</div>
                {this.props.runners.queued.pending.map(i => this._sidebarItem(i))}
                <div className={styles.header}>Created</div>
                {this.props.runners.queued.created.map(i => this._sidebarItem(i))}
                <div className={`${styles.header} ${this.props.runners.queued.manual.length > 0 ? '' : styles.hidden}`}>Manual</div>
                {this.props.runners.queued.manual.map(i => this._sidebarItem(i))}
            </div>
        );
    }

    _sidebarItem(data) {
        return (
            <div key={data.id} className={styles.itemContainer}>
                <div className={styles.itemHeader}>{data.project} | {data.name}</div>
                <div className={styles.info}>
                    <div className={styles.ref}>{data.ref}</div>
                    <div className={styles.time}>{data.created}</div>
                </div>
            </div>
        );
    }
}