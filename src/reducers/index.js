import { combineReducers } from 'redux';

import runnerReducer from './runner.js';
import runnersReducer from './runners.js';
import projectsReducer from './projects.js';

export default combineReducers({
    runner  : runnerReducer,
    runners : runnersReducer,
    projects: projectsReducer
});