const DEFAULT = {
    loading : false,
    error   : false,
    projects: {}
}

const projectsReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'GET_PROJECTS_START': {
            state = Object.assign({}, state, {loading: true});
            break;
        }
        case 'GET_PROJECTS': {
            state = Object.assign({},state, {
                projects: action.data.projects,
                loading : false,
                error: false
            });
            break;
        }
        case 'GET_PROJECTS_ERROR': {
            state = Object.assign({}, state, {loading: false, error: true});
            break;
        }
    }
    return state;
};

export default projectsReducer;