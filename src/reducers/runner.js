const DEFAULT = {
    loading : false,
    apiError: false,
    jobs    : [],
    runner  : {}
}

const runnerReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'GET_RUNNER_START': {
            state = Object.assign({}, state, {loading: true});
            break;
        }
        case 'GET_RUNNER': {
            state = Object.assign({},state, {
                runner: action.data.runner,
                jobs: action.data.jobs,
                loading : false,
                apiError: false
            });
            break;
        }
        case 'GET_RUNNER_ERROR': {
            state = Object.assign({}, state, {loading: false, apiError: true});
            break;
        }
    }
    return state;
};

export default runnerReducer;