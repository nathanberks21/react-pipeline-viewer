import Duration from 'duration';

const DEFAULT = {
    loading: false,
    apiError: false,
    queued: {
        pending: [],
        created: [],
        manual: []
    },
    runners: {
        running: [],
        hasRun: [],
        notRun: []
    }
}

const runnersReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'GET_RUNNERS_START': {
            state = Object.assign({}, state, {loading: true});
            break;
        }
        case 'GET_RUNNERS': {
            const runners = organiseRunners(action.data.runners, action.data.projects);
            const queued = organiseQueue(action.data.queue, action.data.projects);
            state = Object.assign({},state, {
                runners,
                queued,
                loading : false,
                apiError: false
            });
            break;
        }
        case 'GET_RUNNERS_ERROR': {
            state = Object.assign({}, state, {loading: false, apiError: true});
            break;
        }
    }
    return state;
};

export default runnersReducer;

function organiseRunners(runners, projects) {
    const runnersList = [];
    Object.keys(runners).forEach(runner => {
        runnersList.push(runners[runner]);
    });
    const running = runnersList
        .filter(runner => runner.active_job)
        .sort((a, b) => new Date(a.active_job.start_time) < new Date(b.active_job.start_time) ? -1 : 1)
        .map(cardDataMap.bind(projects))
    ;
    const hasRun = runnersList
        .filter(runner => !runner.active_job && runner.last_job && runner.description !== 'Unassigned')
        .sort((a, b) => new Date(a.last_job.finish_time) < new Date(b.last_job.finish_time) ? -1 : 1)
        .map(cardDataMap.bind(projects))
    ;
    const notRun = runnersList
        .filter(runner => !runner.active_job && !runner.last_job && runner.description !== 'Unassigned')
        .map(cardDataMap.bind(projects))
        .sort((a, b) => a.description.toLowerCase() < b.description.toLowerCase() ? -1 : 1)
    ;
    return {running, hasRun, notRun};
}

function organiseQueue(queue, projects) {
    const pending = [];
    const created = [];
    const manual = [];

    queue
        .sort((a, b) => new Date(a.created_at) < new Date(b.created_at) ? -1 : 1)
        .forEach(item => {
            const project = projects[item.project_id].name_with_namespace;
            const obj = {
                id      : item.id,
                created : Duration(new Date(item.created_at), new Date()).toString(1, 1),
                name    : item.name,
                ref     : item.ref,
                project
            };
            if (item.status === 'pending') {
                pending.push(obj);
            } else if (item.status === 'created') {
                created.push(obj);
            } else if (item.status === 'manual') {
                manual.push(obj);
            }
        })
    ;
    return {pending, created, manual};
}

function cardDataMap(data) {
    let time;
    let url;
    let project;
    let ref;
    let name;
    let jobUrl;
    if (data.active_job) {
        time = Duration(new Date(data.active_job.start_time), new Date()).toString(1, 1);
        url = this[data.active_job.project_id].web_url;
        project = data.active_job.project_name;
        ref = data.active_job.ref;
        name = data.active_job.name;
        jobUrl = data.active_job.job_url;
    } else if (data.last_job) {
        time = `${Duration(new Date(data.last_job.finish_time), new Date()).toString(1, 2)} ago`;
        url = this[data.last_job.project_id].web_url;
        project = data.last_job.project_name;
        ref = data.last_job.ref;
        name = data.last_job.name;
        jobUrl = data.last_job.job_url;
    } else {
        url = this[data.projects[0]].web_url;
        project = this[data.projects[0]].name_with_namespace;
    }
    return {
        id: data.id,
        active: data.active,
        ref,
        name,
        description: data.description,
        meanJobs: data.jobs_per_day,
        url,
        jobUrl,
        project,
        runResult: data.last_job ? data.last_job.status : null,
        time
    };
}