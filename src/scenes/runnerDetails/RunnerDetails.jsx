import React from 'react';
import { connect } from 'react-redux';

import styles from './RunnerDetails.css';

import * as Api from '../../actions/apiActions.js';

import ApiError from '../../components/apiError/ApiError.jsx';
import Loader from '../../components/loader/Loader.jsx';

const POLL_PERIOD = 1000;

@connect((store) => {
    return {
        runner: store.runner
    }
})
export default class RunnerDetailsView extends React.Component {
    componentWillMount() {
        this.interval = setInterval(this._getData.bind(this), POLL_PERIOD);
        this._getData();
    }

    render() {
        return (
            <div className={styles.container}>
                {this._view()}
                <Loader visible={this.props.loading}/>
            </div>
        );
    }

    _view() {
        if (this.props.apiError) {
            return (<ApiError />);
        }
        return (<div>{this._runnerTitle()}</div>);
    }

    _runnerTitle() {
        const runner = this.props.runner.runner;
        if (!runner) { return; }
        const tags = runner.tag_list ? runner.tag_list : [];
        const header = <div className={styles.header}>
            <a href='/' className={styles.link}><i className="fa fa-arrow-circle-o-left" aria-hidden="true"></i></a>
            <h2>{runner.description}</h2>
            {tags.map(tag => <span className={styles.tag} key={tag}>{tag}</span>)}
        </div>;
        if (!runner.active) {
            return (<div>{header}<p>Runner {runner.id} is currently <b className={styles.red}>INACTIVE</b></p></div>);
        } if (runner.active_job) {
            return (<div>{header}<p>Runner {runner.id} is currently <b className={styles.green}>ACTIVE</b> and has been running a job since {runner.active_job.start_time}</p></div>);
        }
        return (<div>{header}<p>Runner {runner.id} is currently <b className={styles.green}>ACTIVE</b></p></div>);
    }

    _getData() {
        const id = this.props.match.params.id;
        this.props.dispatch(Api.getRunner(id));
    }

    componentWillUnmount() {
        if (this.interval) {
            window.clearInterval(this.interval);
        }
    }
}