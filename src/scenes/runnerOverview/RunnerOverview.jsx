import React from 'react';
import { connect } from 'react-redux';

import styles from './RunnerOverview.css';

import * as Api from '../../actions/apiActions.js';

import RunnerCard from '../../components/runnerCard/RunnerCard.jsx';
import ApiError from '../../components/apiError/ApiError.jsx';
import Loader from '../../components/loader/Loader.jsx';
import Sidebar from '../../components/sidebar/Sidebar.jsx';

const POLL_PERIOD = 1000;

@connect((store) => {
    return {
        runners: store.runners
    }
})
export default class RunnerOverview extends React.Component {
    componentWillMount() {
        this.interval = setInterval(this._getData.bind(this), POLL_PERIOD);
        this._getData();
    }

    render() {
        return (
            <div className={styles.container}>
                {this._view()}
                <Sidebar />
                <Loader visible={this.props.runners.loading}/>
            </div>
        );
    }

    _view() {
        if (this.props.runners.apiError) {
            return (
                <ApiError />
            );
        }
        return (
            <div className={styles.runnerList}>
                <h3>Running</h3>
                <div className={styles.listContainer}>
                    {this.props.runners.runners ? this._renderList(this.props.runners.runners.running, true) : ''}
                </div>
                <h3>Previously Ran</h3>
                <div className={styles.listContainer}>
                    {this.props.runners.runners ? this._renderList(this.props.runners.runners.hasRun, false) : ''}
                </div>
                <h3>Not Run</h3>
                <div className={styles.listContainer}>
                    {this.props.runners.runners ? this._renderRunner(this.props.runners.runners.notRun, false) : ''}
                </div>
            </div>
        );
    }

    _renderList(runners, running) {
        let projects = {};
        runners.forEach(runner => {
            if (!(runner.project in projects)) {
                projects[runner.project] = [];
            }
            projects[runner.project].push(runner);
        });
        const keys = Object.keys(projects);
        keys.sort((a, b) => projects[a].length > projects[b].length);
        return keys.map((key, i) =>
            <div key={i} className={styles.runnersContainer}>
                <p>{key}</p>
                {this._renderRunner(projects[key], running)}
            </div>
        );
    }

    _renderRunner(runners, running) {
        return (
            runners.map(runner => 
                <RunnerCard 
                    key={runner.id}
                    id={runner.id}
                    running={running}
                    active={runner.active}
                    description={runner.description}
                    jobUrl={runner.jobUrl}
                    url={runner.url}
                    jobsPerDay={runner.meanJobs}
                    runResult={runner.runResult}
                    time={runner.time}
                    reference={runner.ref}
                    name={runner.name}/>
            )
        );
    }

    _getData() {
        this.props.dispatch(Api.getRunners());
    }

    componentWillUnmount() {
        if (this.interval) {
            window.clearInterval(this.interval);
        }
    }
}