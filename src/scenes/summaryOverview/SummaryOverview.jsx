import React from 'react';
import { connect } from 'react-redux';

import styles from './SummaryOverview.css';

import * as Api from '../../actions/apiActions.js';

import ApiError from '../../components/apiError/ApiError.jsx';
import Loader from '../../components/loader/Loader.jsx';

const POLL_PERIOD = 1000;

@connect((store) => {
    return {
        projects: store.projects,
        runners: store.runners
    }
})
export default class SummaryOverview extends React.Component {
    componentWillMount() {
        this.interval = setInterval(this._getData.bind(this), POLL_PERIOD);
        this._getData();
    }

    render() {
        return (
            <div className={`${styles.container} ${styles.coloured}`}>
                {this._view()}
                <Loader visible={this.props.runners.loading}/>
            </div>
        );
    }

    _view() {
        if (this.props.apiError) {
            return (<ApiError />);
        }
        if (this.props.projects.projects && this.props.runners.runners) {
            let runners = this.props.runners.runners.running;
            let queued = this.props.runners.queued;
            let projects = this.props.projects.projects;
            let projectSummaries = {};
            Object.keys(projects).forEach(key => {
                projectSummaries[projects[key].name_with_namespace] = {
                    running : [],
                    pending : [],
                    created : [],
                    manual  : []
                };
            });
            runners.forEach(runner => {
                if (runner.project in projectSummaries) {
                    projectSummaries[runner.project].running.push(runner);
                }
            });
            queued.pending.forEach(job => {
                if (job.project in projectSummaries) {
                    projectSummaries[job.project].pending.push(job);
                }
            });
            queued.created.forEach(job => {
                if (job.project in projectSummaries) {
                    projectSummaries[job.project].created.push(job);
                }
            });
            queued.manual.forEach(job => {
                if (job.project in projectSummaries) {
                    projectSummaries[job.project].manual.push(job);
                }
            });

            return (
                Object.keys(projectSummaries).map(key => this._renderCard(key, projectSummaries[key]))
            );
        }
    }

    _getData() {
        this.props.dispatch(Api.getProjects());
        this.props.dispatch(Api.getRunners());
    }

    _renderCard(name, stats) {
        return(
            <div key={name} className={styles.card}>
                <div className={styles.header}>{name}</div>
                <div className={styles.stats}>
                    <div className={`${styles.stat} ${styles.major}`}>
                        <div className={styles.value}>{stats.running.length}</div>
                        <div className={styles.context}>Running</div>
                    </div>
                    <div>
                        <div className={`${styles.stat} ${styles.medium}`}>
                            <div className={styles.value}>{stats.pending.length}</div>
                            <div className={styles.context}>Pending</div>
                        </div>
                        <div className={styles.minor}>
                            <div className={styles.stat}>
                                <div className={styles.value}>{stats.created.length}</div>
                                <div className={styles.context}>Created</div>
                            </div>
                            <div className={styles.stat}>
                                <div className={styles.value}>{stats.manual.length}</div>
                                <div className={styles.context}>Manual</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    componentWillUnmount() {
        if (this.interval) {
            window.clearInterval(this.interval);
        }
    }
}